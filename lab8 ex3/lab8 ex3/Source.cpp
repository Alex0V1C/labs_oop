#include <iostream>
#include <math.h>
#include <algorithm>
#include <chrono>
#include <time.h>
#include <random>

using namespace std;

class NrComplex
{
public:
	double x, y;

	NrComplex() {}

	NrComplex(double x, double y)
	{
		this->x = x;
		this->y = y;
	}

	void afisarea()
	{
		cout << this->x << " + (" << this->y << ")i";
	}


};

ostream &operator<<(ostream &output, const NrComplex &c)
{
	output << "Numar complex: " << c.x << " + " << c.y << "i" << endl;
	return output;
}  

template <class T>
T returnTip(T val)
{	
	return val;
}

int main()
{	
	srand(time(NULL));
	int x = rand() % 3 + 1;

	if (x == 1)
	{
		int nr = rand() % 100;
		cout << "Numar intreg: " << returnTip(nr) << "\n";
	}
	else if (x == 2)
	{
		uniform_real_distribution<double> unif(0, 100);
		default_random_engine re;
		double nr = unif(re);
		cout << "Numar real: " << returnTip(nr) << "\n";
	}
	else if (x == 3)
	{
		uniform_real_distribution<double> unif(0, 100);
		default_random_engine re;
		double a = unif(re);
		double b = unif(re);
		NrComplex z = NrComplex(a, b);
		cout << returnTip(z);
	}

	srand(time(NULL));

	system("pause");
	return 0;
}