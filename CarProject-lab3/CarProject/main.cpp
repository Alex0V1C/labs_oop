#include <iostream>
#include <string>
using namespace std;

class Car
{
private:
	string name;
	float weight; // in tones

	Car(string name = "Dacia", float weight = 0.5f) {
		this->name = name;
		this->weight = weight;
	}

public:
	static Car createCar();
	static Car createCar(string, float);

	void print() {
		cout << string(20, '-') << "\n";
		cout << "Name: " << name << "\n";
		cout << "Weight: " << weight << "\n";
		cout << string(20, '-') << "\n";
	}
};

Car Car::createCar() {
	return Car();
}

Car Car::createCar(string name, float weight) {
	return Car(name, weight);
}

int main()
{
	Car car1 = Car::createCar();
	Car car2 = Car::createCar("Volkswagen", 1.4);

	car1.print();
	car2.print();

	system("pause");

	return 0;
}
