#include <iostream>

using namespace std;

class Weapon
{
protected:
	int level;
	int power;

public:
	Weapon(int level, int power)
	{
		this->level = level;
		this->power = power;
	}

	virtual void increasePower() = 0;
	virtual void nextLevel() = 0;
	
	virtual ~Weapon() = 0;
};
//Daca nu implementam destructorul programul nu functioneaza
Weapon::~Weapon()
{
	cout << "The weapon was deleted with pure virtual destructor\n";
}

class Sling: public Weapon
{
public:
	Sling(int level, int power) :Weapon(level, power) {}

	void increasePower()
	{
		this->power++;
	}

	void nextLevel()
	{
		Sling::increasePower();
		this->level;
	}

	int getLevel()
	{
		return this->level;
	}

	int getPower()
	{
		return this->level;
	}

	~Sling()
	{
		cout << "The weapon was deleted\n";
	}
};

class AtomicBomb :public Weapon
{
public:
	AtomicBomb(int level, int power) : Weapon(level, power) {}
	
	void increasePower()
	{
		this->power += 50;
	}

	void nextLevel()
	{
		AtomicBomb::increasePower();
		this->level;
	}

	int getLevel()
	{
		return this->level;
	}

	int getPower()
	{
		return this->level;
	}
	
	~AtomicBomb()
	{
		cout << "The weapon was deleted\n";
	}
};

class Gun : public Weapon
{
protected:
	int nrBullets;

public:
	Gun(int level, int power, int nrBullets) :Weapon(level, power)
	{
		this->nrBullets = nrBullets;
	}
	
	virtual void increaseNrBullets() = 0;

	void increasePower()
	{
		this->power += 2;
	}

	void nextLevel()
	{
		Gun::increasePower();
		this->level;
	}

	int getLevel()
	{
		return this->level;
	}

	int getPower()
	{
		return this->level;
	}

	int getNrBullets()
	{
		return this->nrBullets;
	}
	~Gun()
	{
		cout << "The weapon was deleted\n";
	}
};
void Gun::increaseNrBullets()
{
	this->nrBullets += 5;
}

class Rifle : public Gun
{
public:
	Rifle(int level, int power, int nrBullets) :Gun(level, power, nrBullets) {}

	using Gun::getLevel;
	using Gun::getPower;
	using Gun::nextLevel;
	using Gun::increasePower;
	using Gun::getNrBullets;

	void increaseNrBullets()
	{
		this->nrBullets += 50;
	}
	
	~Rifle()
	{
		cout << "The weapon was deleted\n";
	}
};

class  MachineGun : public Rifle
{
protected:
	int speed;

public:
	MachineGun(int level, int power, int nrBullets, int speed) : Rifle(level, power, nrBullets)
	{
		this->speed = speed;
	}
	
	using Rifle::getLevel;
	using Rifle::getPower;
	using Rifle::nextLevel;
	using Rifle::increasePower;
	using Rifle::increaseNrBullets;
	using Rifle::getNrBullets;

	int getSpeed()
	{
		return this->speed;
	}
	~MachineGun()
	{
		cout << "The weapon was deleted\n";
	}
};

int main()
{	
	//Weapon *weapon = new Weapon(2, 1); error

	Sling *sling = new Sling(4, 2);
	sling->increasePower();
	cout << "Sling Level / Power: " << sling->getLevel() << " " << sling->getPower() << "\n";
	sling->nextLevel();
	cout << "Sling Level / Power: " << sling->getLevel() << " " << sling->getPower() << "\n";
	sling->~Sling();

	AtomicBomb *AB = new AtomicBomb(2, 5);
	AB->nextLevel();
	cout << "AtomicBomb Level / Power: " << AB->getLevel() << " " << AB->getPower() << "\n";
	
	//ERROR
	/*Gun *gun = new Gun(2, 5, 10);
	gun->increaseNrBullets();
	cout << "Gun Level / Power / NrBullets: " << gun->getLevel() << " " << gun->getPower() << " " << gun->getNrBullets() << "\n";
	*/

	Rifle *rifle = new Rifle(5, 15, 10);
	rifle->increaseNrBullets();
	cout << "Rifle Level / Power / NrBullets: " << rifle->getLevel() << " " << rifle->getPower() << " " << rifle->getNrBullets() << "\n";

	MachineGun *MG = new MachineGun(10, 2, 5, 12);
	MG->increaseNrBullets();
	MG->nextLevel();
	cout << "MachineGun Level / Power / NrBullets: " << MG->getLevel() << " " << MG->getPower() << " " << MG->getNrBullets() << " " << MG->getSpeed() << "\n";
	
	system("pause");
	return 0;

}