
// Clasa Desert este clasa de baza, clasa Prajitura mosteneste clasa Desert.
// Clasa TortAniversar mosteneste clasa Prajitura

#include <iostream>
#include <string>

using namespace std;

class Desert
{
protected:
	string ingredientSecret;

public:
	string nume;
	int Kcal; // Numar Kilocalorii / 100 grame

	Desert(string nume, string ingredientSecret, int Kcal);
	~Desert();

	string getName();
	int getKcal();
	void displayName();
};

class Prajitura :public Desert
{
public:
	string tipGlazura;
	int timpCoacere; // in minute

	Prajitura(string nume, string ingredientSecret, int Kcal, string tipGlazura, int timpCoacere);
	//{} :Desert(nume, ingredientSecret, Kcal) {};
	~Prajitura();

	using Desert::getName;
	using Desert::getKcal;
	using Desert::displayName;

	//string getName1() {};
	//string getKcal1() {};

	string getTipGlazura();
	int getTimpCoacere();
};

class TortAniversare : public Prajitura
{
protected:
	string numeSarbatorit;
	int varstaSarbatorit;
	string urare;

public:
	int nrEtaje;
	string culoareGlazura;
	string tipDecoratiuni;

	TortAniversare(string nume, string ingredientSecret, int Kcal, string tipGlazura, int timpCoacere, string numeSarbatorit,
		int varstaSarbatorit, string urare, int nrEtaje, string culoareGlazura, string tipDecoratiuni);
	// :Prajitura(nume, ingredientSecret, Kcal, tipGlazura, timpCoacere) {};

	~TortAniversare();

	/*string getName1() {};
	string getKcal1() {};

	string getTipGlazura2() {};
	int getTimpCoacere12() {};
	*/

	using Prajitura::getName;
	using Prajitura::getKcal;
	using Desert::displayName;

	using Prajitura::getTipGlazura;
	using Prajitura::getTimpCoacere;

	int getVarstaSarbatorit();
	string getNumeSarbatorit();
	string getUrare();
	int getNrEtaje();
	string getCuloareGlazura();
	string getTipDecoratiuni();

};
3
