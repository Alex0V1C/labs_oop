#include <iostream>
#include "Header.h"
#include <string>

using namespace std;

int main()
{

	Desert *D1 = new Desert("Acadea", "zahar brun", 55);
	Prajitura *P1 = new Prajitura("Negresa", "Afine", 90, "Ciocolata", 30);
	TortAniversare *T1 = new TortAniversare("Tort cu fructe", "Kaki & miere", 150, "Vanilie", 65, "Alex", 20, "La multi ani!", 3, "Verde", "Trandafiri");

	//Testam cativa getteri
	cout << D1->getKcal() << "\n";
	cout << P1->getTipGlazura() << "\n";
	cout << T1->getVarstaSarbatorit() << "\n\n";

	//Testam functia displayName:
	D1->displayName();
	P1->displayName();
	T1->displayName();

	system("pause");
	return 0;
}