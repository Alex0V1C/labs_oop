#include "Header.h"								

										// Implementarea functiilor:

// Fuctii din clasa Desert:
Desert::Desert(string nume, string ingredientSecret, int Kcal) {
	this->nume = nume;
	this->ingredientSecret = ingredientSecret;
	this->Kcal = Kcal;
}

Desert::~Desert()
{
	cout << "Desert sters/distrus\n";
}

string Desert::getName()
{
	return this->nume;
}


int Desert::getKcal()
{
	return this->Kcal;
}

void Desert::displayName()
{
	cout << this->nume << "\n";
}


//Functii din clasa Prajitura:
Prajitura::Prajitura(string nume, string ingredientSecret, int Kcal, string tipGlazura, int timpCoacere)
	:Desert(nume, ingredientSecret, Kcal) 
{
	this->tipGlazura = tipGlazura;
	this->timpCoacere = timpCoacere;
}

Prajitura::~Prajitura()
{
	cout << "Prajitura stearsa/distrusa\n";
}

string Prajitura::getTipGlazura()
{
	return this->tipGlazura;
}

int Prajitura::getTimpCoacere()
{
	return this->timpCoacere;
}


//Functii din clasa TortAniversare:
TortAniversare::TortAniversare(string nume, string ingredientSecret, int Kcal, string tipGlazura, int timpCoacere, string numeSarbatorit,
	int varstaSarbatorit, string urare, int nrEtaje, string culoareGlazura, string tipDecoratiuni)
	:Prajitura(nume, ingredientSecret, Kcal, tipGlazura, timpCoacere)
{
	this->varstaSarbatorit = varstaSarbatorit;
	this->numeSarbatorit = numeSarbatorit;
	this->urare = urare;
	this->nrEtaje = nrEtaje;
	this->culoareGlazura = culoareGlazura;
	this->tipDecoratiuni = tipDecoratiuni;
}

TortAniversare::~TortAniversare()
{
	cout << "Tort sters/distrus";
}

int TortAniversare::getVarstaSarbatorit()
{
	return this->varstaSarbatorit;
}

string TortAniversare::getNumeSarbatorit()
{
	return this->numeSarbatorit;
}

string TortAniversare::getUrare()
{
	return this->urare;
}

string TortAniversare::getCuloareGlazura()
{
	return this->culoareGlazura;
}

string TortAniversare::getTipDecoratiuni()
{
	return this->tipDecoratiuni;
}

int TortAniversare::getNrEtaje()
{
	return this->nrEtaje;
}

