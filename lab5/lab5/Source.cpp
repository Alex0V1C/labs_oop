#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Form
{
protected:
	string name;
	string color;
	
	void setColor(string color)
	{
		this->color = color;
	}

	string getColor()
	{
		return this->color;
	}

public:
	Form(string name, string color)
	{
		this->name = name;
		this->color = color;
	}

	~Form()
	{
		cout << "Delete form" << "\n";
	}

	string getName()
	{
		return this->name;
	}

	void setName(string name)
	{
		this->name = name;
	}

	void showMessage()
	{
		cout << "This is a message for Form class" << "\n";
	}

	int computeArea()
	{
		return 0;
	}

	int computePerimeter()
	{
		return 0;
	}

};

class Rectangle :public Form
{
protected:
	int width;
	int height;

public:
	Rectangle(string name, string color, int height, int width) :Form(name, color)
	{
		this->height = height;
		this->width = width;
	}

	~Rectangle()
	{
		cout << "Delete rectangle" << "\n";
	}

	void setWidth (int width)
	{
		this->width = width;
	}

	int getWidth()
	{
		return this->width;
	}

	void setHeight (int height)
	{
		this->height = height;
	}

	int getHeight()
	{
		return this->height;
	}


	void setColorOfRectangle(string color)
	{
		Form::setColor(color);
	} 
	//OR
	//using Form::setColor();

	string getColorOfRectangle()
	{
		return Form::getColor();
	}

	void showMessage()
	{
		Form::showMessage();
		cout << "This is a message for Rectangle class" << "\n";
	}


	int computeArea()
	{
		return (this->width * this->height);
	}

	int computePerimeter()
	{
		return (2 * this->width + 2 * this->height);
	}
};

class Square :public Form
{
private:
	int side;

public:
	Square(string name, string color, int side) :Form(name, color)
	{
		this->side = side;
	}

	~Square()
	{
		cout << "Delete square" << "\n";
	}

	void setSide(int side)
	{
		this->side = side;
	}

	int getSide()
	{
		return this->side;
	}

	void showMessage()
	{	
		Form::showMessage();
		cout << "This is a message for Square class" << "\n";
	}

	int computeArea()
	{
		return pow(this->side, 2);
	}

	int computePerimeter()
	{
		return (4 * this->side);
	}
};

int main()
{	// We use setters and getters!!!

	/*Form *f = new Form();
	f->setName("formal");
	//f->setColor("red");
	
	cout << "forma: " << f->getName() << "\n";

	Rectangle *r = new Rectangle();
	r->setHeight(32);
	r->setWidth(16);
	r->setName("rectangle 1");
	r->setColorOfRectangle("red");
	
	cout << r->getName() << " " << r->getHeight() << " " << r->getWidth()  << " " << r->getColorOfRectangle()<< "\n";

	Rectangle r2;
	r2.setHeight(40);
	r2.setWidth(38);
	r2.setName("rectangle 2");
	r2.setColorOfRectangle("blue");

	cout << r2.getName() << " " << r2.getHeight() << " " << r2.getWidth() << " " << r2.getColorOfRectangle() << "\n";
	*/

	//---------------------------------------------------//

	//Now we use constructors instead of getters!!!

	Form *f = new Form("form", "red");
	Rectangle *r = new Rectangle("rectangle", "blue", 40, 25);
	Square s = Square("square", "orange", 9);


	cout << "1. Now we try to use destructors:" << "\n";
	cout << "Form: ", f->~Form();
	cout << "Rectangle: ", r->~Rectangle(); // ~Rectangle is the first being used, ~Form the second
	cout << "Square: ", s.~Square();
	cout << "\n\n";

	cout << "2. Now we try to use showMesage() function: " << "\n";
	cout << "Form: ", f->showMessage();
	cout << "Rectangle: ", r->showMessage();
	cout << "Square: ", s.showMessage();
	cout << "\n\n";

	cout << "3. We calculate the Area and Perimeter: \n";
	cout << "Form: "<< f->computeArea() << "   " << f->computePerimeter() << "\n";
	cout << "Rectangle: " << r->computeArea() << "   " << r->computePerimeter() << "\n";
	cout << "Square: " << s.computeArea() << "   " << s.computePerimeter() << "\n";
	cout << "\n\n";

	system("pause");

	return 0;
}