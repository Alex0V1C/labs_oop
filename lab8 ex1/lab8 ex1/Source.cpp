#include <iostream>
#include <cmath>
#include <algorithm>

using namespace std; 

template <class T> 
T GetMax(T a, T b)
{
	T result;
	result = (a>b) ? a : b;
	return (result);
}

template <class T>
T GetMin(T a, T b)
{
	T ans = (a < b) ? a : b;
	return ans;
}

class NrComplex
{
public:
	double parteReala, parteImaginara, distanta;
	NrComplex(int a, int b)
	{
		this->parteReala = a;
		this->parteImaginara = b;
		this->distanta = sqrt(pow(this->parteImaginara, 2) + pow(this->parteReala, 2));
	}

	void afisare()
	{
		cout << this->parteReala << " + ( " << this->parteImaginara << " )i" << endl;
	}
};

int main()
{
	//int i = 5, j = 6, k;
	//long l = 10, m = 5, n;
	
	//k = GetMax<int>(i, j);
	//n = GetMax<long>(l, m);

	NrComplex nr1(2, 4);
	NrComplex nr2(3, 6);

	double Max = GetMax<double>(nr1.distanta, nr2.distanta);
	double Min = GetMin<double>(nr1.distanta, nr2.distanta);
	cout << Max << endl << Min;
	//cout << k << endl;
	//cout << n << endl;
	 
	system("pause");
	return 0;
}