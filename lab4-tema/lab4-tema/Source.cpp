#include <iostream>
#include <list>
#include <string>
#include <algorithm>
using namespace std;

class Student {
private:
	int note;
	string name;

public:
	Student(int note = 0, string name = "Joe Doe") {
		this->note = note;
		this->name = name;
	}

	void setNote(int n) {
		this->note = n;
	}

	int getNote() {
		return this->note;
	}

	string getName() {
		return this->name;
	}

	void setName(string name) {
		this->name = name;
	}

	void printStudent() {
		cout << this->name << " " << this->note << "\n";
	}
};

class StudentsGroup {
private:
	int studentsNumber;
	list<Student> studentsList;

public:
	StudentsGroup(int studentsNumber) {
		this->studentsNumber = studentsNumber;
	}

	void showStudentsInGroup() {
		int cnt = 0;
		for (Student student : studentsList) {
			student.printStudent();
		}
		cout << "\n\n";
	}

	void readStudentGroup() {
		int note;
		string studentName;

		for (int i = 0; i < this->studentsNumber; i++) {
			cout << "student " << i << " name:";
			cin >> studentName;
			cout << "note ";
			cin >> note;

			/*studentsList[i].setNote(note);
			studentsList[i].setName(studentName);*/

			studentsList.push_back(Student(note, studentName));
		}
	}

	static bool compByName(Student lhs, Student rhs) {
		return lhs.getName() < rhs.getName();
	}

	static bool compByNote(Student lhs, Student rhs) {
		return lhs.getNote() < rhs.getNote();
	}

	void sortByName() {
		studentsList.sort(StudentsGroup::compByName);
	}

	void sortByNote() {
		studentsList.sort(StudentsGroup::compByNote);
	}

	Student getMaxNote() {
		sortByNote();
		return studentsList.back();
	}

	static bool is5(Student student) {
		return student.getNote() == 5;
	}

	Student getFirst5() {
		/*list<Student>::iterator it = find_if(studentsList.begin(), studentsList.end(),
		[] (Student student) {
		return bool(student.getNote() == 5);
		});*/

		list<Student>::iterator it = find_if(studentsList.begin(), studentsList.end(), StudentsGroup::is5);

		if (it != studentsList.end()) 
			return *it;
		else {
			cout << "No student has note 5!\n";
			return Student(-1, "");
		}
	}
};

int main() {
	StudentsGroup *studentsGroup = new StudentsGroup(3);
	studentsGroup->readStudentGroup();
	studentsGroup->showStudentsInGroup();

	studentsGroup->sortByName();
	studentsGroup->showStudentsInGroup();

	studentsGroup->sortByNote();
	studentsGroup->showStudentsInGroup();

	studentsGroup->getMaxNote().printStudent();

	studentsGroup->getFirst5().printStudent();

	system("pause");

	return 0;
}