#include <iostream> 

using namespace std;

class BaseClass
{
public:
	virtual void a() = 0;  
						  
};

void BaseClass::a() 
{  
	cout << "BaseClass pure virtual a()" << endl;
} 

class B : public BaseClass 
{
public:     
	void a() 
	{     
		BaseClass::a();   
		cout<<"class B method a()"<<endl;
	}
}; 
	
int main() 
{
	BaseClass *baseClass;  
	B *b = new B();

	baseClass = b;  
	baseClass->a();
	b->a();

	system("pause");
	return 0;
}