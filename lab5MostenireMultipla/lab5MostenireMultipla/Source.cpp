#include <iostream>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

//Definiti o clasa Persoana si una Sofer cu membrii variabile, specifici (nume, respectiv seriePermis).
//Fiecare clasa are cate o metoda, cu nume diferit, pentru afisarea informatiilor.

class Person
{
protected:
	string name;

public:
	Person(string name)
	{
		this->name = name;
	}

	void displayName()
	{
		cout << "Name: " << this->name << "\n";
	}
	
	string getName()
	{
		return this->name;
	}
};

class Driver
{
protected:
	string driverLicense;

public:
	Driver(string driverLicense)
	{
		this->driverLicense = driverLicense;
	}

	void displayDriverLicense()
	{
		cout << "Driver license: " << this->driverLicense << "\n";
	}
};


class Employee :public Person, public Driver
{
protected:
	string employer;

public:

	//consructor
	Employee(string name, string driverLicense, string employer) :Person(name), Driver(driverLicense)
	{
		this->employer = employer;
	}

	void displayData()
	{
		Person::displayName();
		Driver::displayDriverLicense();

		cout << this->employer;
		cout << "\n";
	}
	
	static Employee SerchByName(string name);
};

list<Employee> listOfEmployees;

Employee Employee::SerchByName(string Name)
{
	list<Employee>::iterator it;
	for (it = listOfEmployees.begin(); it != listOfEmployees.end(); ++it)
	{
		if (it->Person::getName() == Name)
			return *it;
	}
	return Employee("-1", "-1", "-1");
}



int main()
{
	int i, n;
	cout << "Number of employees: ", cin >> n;

	string name, dLicense, employer;

	for (i = 0; i < n; ++i)
	{
		cout << "\nEnter the name: ", cin >> name;
		cout << "\nEnter the driver license: ", cin >> dLicense;
		cout << "\nEnter the employer's name: ", cin >> employer;

		//Employee e = Employee(name, dLicense, employer);
		listOfEmployees.push_back(Employee(name, dLicense, employer));
	}

	list<Employee>::iterator it;
	int k = 0;

	cout << "List of employees:\n";
	for (it = listOfEmployees.begin(); it != listOfEmployees.end(); ++it)
	{
		++k;
		it->displayData();
		cout << "\n";
	}

	cout << "\n";

	//We serch in list an employee by name
	Employee *p = new Employee("-1", "-1", "-1");
	p->SerchByName("Alexandra").displayData();

	system("pause");

	return 0;
}