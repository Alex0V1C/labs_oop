#include <iostream>
#include <list>
#include <string>
using namespace std;

class Persoana
{
public:
	Persoana() {};


	class Sofer
	{
	protected:
		string nume;
		int varsta;

	public:
		static list<Sofer> soferi;

		Sofer(string nume, int varsta)
		{
			this->nume = nume;
			this->varsta = varsta;

			soferi.push_back(*this);
		}

		void updateNume(string numeNou, int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}

			it->nume = numeNou;
		}

		void updateVarsta(int varstaNoua, int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}

			it->nume = varstaNoua;
		}

		void displayData()
		{
			cout << this->nume << ", " << this->varsta << "\n";
		}

		void displayList()
		{
			for (list<Sofer>::iterator it = soferi.begin(); it != soferi.end(); ++it)
			{
				it->displayData();
			}

			cout << "\n";
		}

		void deleteSofer(int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}
			soferi.erase(it);
		}
	};

	friend class Student;
};

list<Persoana::Sofer> Persoana::Sofer::soferi;

class Student
{
protected:
	string nume;
	int varsta;
	bool carnet;

public:
	Student(string nume, int varsta, bool carnet)
	{
		this->nume = nume;
		this->varsta = varsta;
		this->carnet = carnet;

		if (carnet == true)
			Persoana::Sofer(nume, varsta);
	}

	void displayData()
	{
		cout << this->nume << ", " << this->varsta;
		(this->carnet == true) ? cout << " , are carnet" : cout << " , nu are carnet";
		cout << "\n";
	}

	friend class GrupaStudenti;
};

class GrupaStudenti
{
public:
	list<Student> grupa;
	string numeGrupa;
	int an;

	GrupaStudenti(string numeGrupa, int an, int nr)
	{
		this->numeGrupa = numeGrupa;
		this->an = an;

		for (int i = 0; i < nr; ++i)
		{
			string numeStudent;
			int varstaStudent;
			bool carnet;
			cout << "Nume student: ", cin >> numeStudent;
			cout << "Varsta student: ", cin >> varstaStudent;
			cout << "Studentul are carnet? ", cin >> carnet;
			cout << "\n";

			this->grupa.push_back(Student(numeStudent, varstaStudent, carnet));
		}
	}

	void updateNume(string numeNou, int pos)
	{
		int nr = 0;

		auto it = grupa.begin();
		for (it = grupa.begin(); it != grupa.end() && nr < pos; ++it, ++nr) {}

		it->nume = numeNou;
	}

	void updateVarsta(int varstaNoua, int pos)
	{
		int nr = 0;

		auto it = grupa.begin();
		for (it = grupa.begin(); it != grupa.end() && nr < pos; ++it, ++nr) {}

		it->varsta = varstaNoua;
	}

	void displayList()
	{
		cout << "Grupa " << this->numeGrupa << " " << this->an << " contine urmatorii studenti:\n";
		int k = 0;
		for (list<Student>::iterator it = this->grupa.begin(); it != this->grupa.end(); ++it)
		{
			++k;
			cout << k << ". ", it->displayData();
		}
		cout << "\n";
	}

	void updateAnDeStudiu()
	{
		++this->an;
	}

	void deleteStudent(int pos)
	{
		int nr = 0;

		auto it = grupa.begin();
		for (it = grupa.begin(); it != grupa.end() && nr < pos; ++it, ++nr) {}
		grupa.erase(it);
	}

	void addStudent(string nume, int varsta, bool carnet)
	{
		this->grupa.push_back(Student(nume, varsta, carnet));
	}

	void schimbareGrupa(int pos, list<Student> grupaNoua)
	{
		int nr = 0;

		auto it = this->grupa.begin();
		for (it = this->grupa.begin(); it != this->grupa.end() && nr < pos; ++it, ++nr) {}
		grupaNoua.push_back(*it);
		this->grupa.erase(it);
	}
};

int main()
{
	GrupaStudenti *grup1 = new GrupaStudenti("CE", 1, 3);
	grup1->displayList();
	cout << "\n";
	GrupaStudenti *grup2 = new GrupaStudenti("CR", 1, 3);
	grup2->displayList();
	cout << "\n";

	grup1->updateAnDeStudiu();
	grup1->deleteStudent(0);
	grup1->addStudent("CARMEN", 20, true);
	grup2->schimbareGrupa(0, grup1->grupa);

	grup1->displayList();
	grup2->displayList();

	system("pause");
	return 0;
}