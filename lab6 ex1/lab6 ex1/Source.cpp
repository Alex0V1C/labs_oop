﻿#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

class NrComplex
{
public:
	double x, y;

	NrComplex() {}

	NrComplex(double x, double y)
	{
		this->x = x;
		this->y = y;
	}
};

class Operatii : NrComplex
{
public:
	Operatii() {}
	
	double nr1, nr2;
	NrComplex z1, z2;

private:
	Operatii(double a, double b)
	{
		this->nr1 = a;
		this->nr2 = b;
	}
	
	Operatii(NrComplex a, NrComplex b)
	{
		this->z1 = a;
		this->z2 = b;
	}

public:
	static double Sum(double nr1, double nr2)
	{	
		Operatii(nr1, nr2);
		return nr1 + nr2;
	}
	
	static NrComplex Sum(NrComplex z1, NrComplex z2)
	{	
		Operatii(z1, z2);
		NrComplex ans = NrComplex();
		ans.x = z1.x + z2.x;
		ans.y = z1.y + z2.y;
		
		return ans;
	}

	static double Dif(double nr1, double nr2)
	{
		Operatii(nr1, nr2);
		return nr1 - nr2;
	}

	static NrComplex Dif(NrComplex z1, NrComplex z2)
	{
		Operatii(z1, z2);
		NrComplex ans = NrComplex();
		ans.x = z1.x - z2.x;
		ans.y = z1.y - z2.y;

		return ans;
	}

	static double Produs(double nr1, double nr2)
	{
		Operatii(nr1, nr2);
		return nr1 * nr2;
	}

	static NrComplex Produs(NrComplex z1, NrComplex z2)
	{
		Operatii(z1, z2);
		NrComplex ans = NrComplex();
		ans.x = z1.x * z2.x - z1.y * z2.y;
		ans.y = z1.x * z2.y + z1.y * z2.x;
		
		return ans;
	}

	static NrComplex POW(NrComplex z, int p)
	{	
		NrComplex ans = NrComplex();
		ans = z;
		for (int i = 2; i <= p; ++i)
		{
			ans = Produs(ans, z);
		}

		return ans;
	}

	static double Impartire(double nr1, double nr2)
	{
		Operatii(nr1, nr2);
		return nr1 / nr2;
	}

	static NrComplex Conjugat(NrComplex z)
	{
		NrComplex ans = NrComplex();
		ans.x = z.x;
		ans.y = -z.y;

		return ans;
	}

	static NrComplex Impartire(NrComplex z1, NrComplex z2)
	{
		Operatii(z1, z2);
		NrComplex ans = NrComplex();
		ans = Produs(z1, Conjugat(z2));

		ans.x /= pow(z2.x, 2) + pow(z2.y, 2);
		ans.y /= pow(z2.x, 2) + pow(z2.y, 2);

		return ans;
	}

	static double Modul(NrComplex z)
	{
		return sqrt(z.x * z.x + z.y * z.y);
	}

	static double Modul(double nr)
	{
		return abs(nr);
	}

	static void afisareNumar(NrComplex z)
	{
		cout << z.x << " + (" << z.y << ")i";
	}

	static void afisareNumar(double nr)
	{
		cout << nr;
	}

	friend class Mod_Pow;
};

class Mod_Pow: NrComplex
{
public:
	Mod_Pow(double x, double y) :NrComplex(x, y) {}
	
	double operator~ ()
	{
		return Operatii::Modul(*this);
	}

	NrComplex operator^ (int nr)
	{
		return Operatii::POW(*this, nr);
	}
	
	static void afisareNumar(Mod_Pow z)
	{
		cout << z.x << " + (" << z.y << ")i";
	}

};

int main()
{
	NrComplex *z = new NrComplex(3.2, -4.1);
	double nr = -6.3;
	/*
	Operatii *p = new Operatii();

	p->afisareNumar(*z);
	p->afisareNumar(nr);

	cout << p->Modul(*z) << "\n";
	cout << p->Modul(nr) << "\n";
	OR*/
	///////////////

	cout << "z = ", Operatii::afisareNumar(*z), cout << "\n";
	cout << "nr = ", Operatii::afisareNumar(nr), cout << "\n";

	cout << "Modulul lui z = " << Operatii::Modul(*z) << "\n" << "Modulul lui nr = " << Operatii::Modul(nr) << "\n";

	double n1 = Operatii::Sum(14.06, 3.6);
	cout << "nr1 + nr2 = " << n1 << "\n";

	NrComplex z1 = NrComplex(2.2, 5.3);
	NrComplex z2 = NrComplex(1.5, -2.3);
	NrComplex ans = Operatii::Sum(z1, z2);
	Operatii::afisareNumar(z1), cout << " + ", Operatii::afisareNumar(z2), cout << " = ";
	Operatii::afisareNumar(ans), cout << "\n";

	NrComplex z3 = NrComplex(4, 18);
	NrComplex z4 = NrComplex(2, -3);
	ans = Operatii::Impartire(z3, z4);
	Operatii::afisareNumar(z3), cout << " : ", Operatii::afisareNumar(z4), cout << " = ";
	Operatii::afisareNumar(ans), cout << "\n";

	ans = Operatii::Produs(z3, z4);
	Operatii::afisareNumar(z3), cout << " * ", Operatii::afisareNumar(z4), cout << " = ";
	Operatii::afisareNumar(ans), cout << "\n";

	nr = 3;
	ans = Operatii::POW(z4, nr);
	Operatii::afisareNumar(z4), cout << " ^ " << nr << " = ";
	Operatii::afisareNumar(ans), cout << "\n";

	Mod_Pow modPow(2.0, 2.2);
	cout << "Modulul numarului: " << ~modPow << "\n";


	Mod_Pow::afisareNumar(modPow), cout << " ^ " << nr << " = ";
	Operatii::afisareNumar(modPow ^ nr), cout << "\n";

	system("pause");
	return 0;
}