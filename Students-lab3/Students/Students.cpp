// Declarati un constructor private, care seteaza nota unui element de tip class Student la 8.
// Folositi-l ptr a initializa grade in constructorul initial

//Folositi un membru variabila declarat static cu care sa numarati citi studenti sunt creati in acest moment.
//Adresati membrul static in constructor si destructor.

// Creati o lista simplu inlantuita de studenti ( class Student).
//Afisati lista. Proiectati structutra astfel incat sa pot afisa lista plecand de la oricare element al ei. 
//Gasiti un mod simplu de a numara cati studenti s-au creat.
#include <iostream>
#include <list>
#include <cstring>
using namespace std;

class CStudent
{
private:
	char name[10];
	float grade;
	static int nr;

	CStudent(char name[] = "Popescu", float grade = 8.0f) {
		strcpy_s(this->name, name);
		this->grade = grade;
		++nr;
	}

public:

	static CStudent createStudent()
	{
		char name[10];
		float grade;

		cin >> name;
		cin >> grade;
		++nr;

		return CStudent(name, grade);
	}

	~CStudent() { --nr; }

	void displayGrade();
	void displayListOfStudents();
};

int CStudent::nr = 0;

void CStudent::displayGrade()
{
	cout << this->name << " " << this->grade << "\n";
}


class StudentsGroup
{
	list <CStudent> L;
	int nr;

public:
	StudentsGroup(int nr)
	{
		for (int i = 0; i < nr; ++i)
		{
			L.push_back(CStudent::createStudent());
		}

	}

	void displayList()
	{
		list<CStudent>::iterator it;
		int cnt = 0;

		/*for (it = L.begin(); it != L.end(); ++it)
		{
			cout << "Studentul nr. " << ++cnt << ": ";
			it->displayGrade();
		}*/

		for (CStudent student : L) {
			cout << "Studentul nr. " << ++cnt << ": ";
			student.displayGrade();
		}
	}

};

int main()
{
	int num_of_students;
	cin >> num_of_students;

	StudentsGroup group = StudentsGroup(num_of_students);
	group.displayList();

	system("pause");

	return 0;
}
