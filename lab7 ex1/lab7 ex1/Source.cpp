/*Proiectati o structura de 5 clase: Persoana, Sofer, Angajat, Parinte, Copil.
Folosti clase prietene si imbricate
In definitia structurii de clase tineti cont ca:
-un sofer / un angajat / un copil este o persoana;
-un parinte este persoana, sofer, angajat si are unul sau mai multi copii*/

#include <iostream>
#include <list>
#include <string>
using namespace std;

class Persoana
{
public:
	Persoana() {};


	class Sofer
	{
	protected:
		string nume;
		int varsta;
		string tipMasina;

	public:
		static list<Sofer> soferi;

		Sofer(string nume, int varsta, string tipMasina)
		{
			this->nume = nume;
			this->varsta = varsta;
			this->tipMasina = tipMasina;

			soferi.push_back(*this);
		}

		void updateNume(string numeNou, int pos)
		{
			int nr = 0;
			
			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}

			it->nume = numeNou;
		}

		void updateVarsta(int varstaNoua, int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}

			it->nume = varstaNoua;
		}

		void updateTipMasina(string tipNou, int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}

			it->tipMasina = tipNou;
		}

		void displayData()
		{
			cout << this->nume << ", " << this->varsta << ", " << this->tipMasina << "\n";
		}

		void displayList()
		{
			for (list<Sofer>::iterator it = soferi.begin(); it != soferi.end(); ++it)
			{
				it->displayData();
			}

			cout << "\n";
		}

		void deleteSofer(int pos)
		{
			int nr = 0;

			auto it = soferi.begin();
			for (it = soferi.begin(); it != soferi.end() && nr < pos; ++it, ++nr) {}
			soferi.erase(it);
		}
	};

	

	class Angajat
	{
	protected:
		string nume;
		int varsta;
		string locDeMunca;

	public:
		static list<Angajat> angajati;

		Angajat(string nume, int varsta, string locDeMunca)
		{
			this->nume = nume;
			this->varsta = varsta;
			this->locDeMunca = locDeMunca;

			angajati.push_back(*this);
		}

		void updateNume(string numeNou, int pos)
		{
			int nr = 0;

			auto it = angajati.begin();
			for (it = angajati.begin(); it != angajati.end() && nr < pos; ++it, ++nr) {}

			it->nume = numeNou;
		}

		void updateVarsta(int varstaNoua, int pos)
		{
			int nr = 0;

			auto it = angajati.begin();
			for (it = angajati.begin(); it != angajati.end() && nr < pos; ++it, ++nr) {}

			it->varsta = varstaNoua;
		}

		void updateLocDeMunca(string locNou, int pos)
		{
			int nr = 0;

			auto it = angajati.begin();
			for (it = angajati.begin(); it != angajati.end() && nr < pos; ++it, ++nr) {}

			it->locDeMunca = locNou;
		}

		void displayData()
		{
			cout << this->nume << ", " << this->varsta << ", " << this->locDeMunca << "\n";
		}

		void displayList()
		{
			for (list<Angajat>::iterator it = angajati.begin(); it != angajati.end(); ++it)
			{
				it->displayData();
			}

			cout << "\n";
		}

		void deleteAngajat(int pos)
		{
			int nr = 0;

			auto it = angajati.begin();
			for (it = angajati.begin(); it != angajati.end() && nr < pos; ++it, ++nr) {}
			angajati.erase(it);
		}
	};


	class Copil
	{
	protected:
		string nume;
		int varsta;

	public:
		Copil(string nume, int varsta)
		{
			this->nume = nume;
			this->varsta = varsta;
		}

		void displayData()
		{
			cout << this->nume << ", " << this->varsta << "\n";
		}
	};

	friend class Parinte;
};

list<Persoana::Sofer> Persoana::Sofer::soferi;
list<Persoana::Angajat> Persoana::Angajat::angajati;

class Parinte
{
protected:
	string nume;
	int varsta;
	string tipMasina;
	string locDeMunca;
	int nrCopii;

public:
	static list<Parinte> parinti;
	list <Persoana::Copil> copii;

	Parinte(string nume, int varsta, string tipMasina, string locDeMunca, int nrCopii)
	{
		this->nume = nume;
		this->varsta = varsta;
		this->tipMasina = tipMasina;
		this->locDeMunca = locDeMunca;
		this->nrCopii = nrCopii;

		Persoana::Angajat(nume, varsta, locDeMunca);
		Persoana::Sofer(nume, varsta, tipMasina);

		cout << "Introduceti " << nrCopii << " copii" << ":\n";

		for (int i = 1; i <= nrCopii; ++i)
		{
			string numeCopil;
			int varstaCopil;
			cout << "Numele copilului: ", cin >> numeCopil;
			cout << "Varsta copilului: ", cin >> varstaCopil;

			copii.push_back(Persoana::Copil(numeCopil, varstaCopil));
		}

		parinti.push_back(*this);
	}

	void updateNume(string numeNou, int pos)
	{	
		int nr = 0;

		auto it = parinti.begin();
		for (it = parinti.begin(); it != parinti.end() && nr < pos; ++it, ++nr) {}

		it->nume = numeNou;
	}

	void updateVarsta(int varstaNoua, int pos)
	{
		int nr = 0;

		auto it = parinti.begin();
		for (it = parinti.begin(); it != parinti.end() && nr < pos; ++it, ++nr) {}

		it->varsta = varstaNoua;
	}

	void updateTipMasina(string tipNou, int pos)
	{
		int nr = 0;

		auto it = parinti.begin();
		for (it = parinti.begin(); it != parinti.end() && nr < pos; ++it, ++nr) {}

		it->tipMasina = tipNou;
	}

	void updateLocDeMunca(string locNou, int pos)
	{
		int nr = 0;

		auto it = parinti.begin();
		for (it = parinti.begin(); it != parinti.end() && nr < pos; ++it, ++nr) {}

		it->locDeMunca = locNou;
	}

	void displayData()
	{
		cout << this->nume << ", " << this->varsta << ", " << this->locDeMunca << ", " << this->tipMasina << "\n";
		cout << "Nr copii: " << copii.size() << "\n";

		for (list<Persoana::Copil>::iterator it = copii.begin(); it != copii.end(); ++it)
		{
			it->displayData();
		}

		cout << "\n";
	}

	void displayList()
	{
		for (list<Parinte>::iterator it = parinti.begin(); it != parinti.end(); ++it)
		{
			it->displayData();
		}

		cout << "\n";
	}
	
	void deleteParinte(int pos)
	{
		int nr = 0;

		auto it = parinti.begin();
		for (it = parinti.begin(); it != parinti.end() && nr < pos; ++it, ++nr) {}
		parinti.erase(it);
	}
};

list<Parinte> Parinte::parinti;

int main()
{
	//Declaram 3 angajati
	Persoana::Angajat *angajat1 = new Persoana::Angajat("Ion", 23, "MC");
	Persoana::Angajat("Marian", 25, "KFC");
	Persoana::Angajat("Ruxi", 22, "Google");

	cout << "Lista cu angajati:" << "\n", angajat1->displayList(), cout << "\n";

	//Declaram 2 soferi
	Persoana::Sofer *sofer1 = new Persoana::Sofer("Claudia", 18, "Logan");
	Persoana::Sofer("Ilinca", 19, "Toyota");

	cout << "\nLista cu soferi:" << "\n", sofer1->displayList(), cout << "\n";

	//Declaram 4 parinti => Mai apar 4 soferi si 4 angajati
	Parinte *p1 = new Parinte("Alina", 34, "Dacia", "ING", 2);
	Parinte::Parinte("Robert", 40, "Opel", "Teatrul Liric", 1);
	Parinte::Parinte("Alex", 36, "Matiz", "ANAF", 3);
	Parinte::Parinte("Flavius", 38, "Audi", "DNA", 2);

	cout << "\nLista cu parinti: " << "\n", p1->displayList(), cout << "\n";

	cout << "\nNoua lista cu angajati:\n", angajat1->displayList(), cout << "\n";
	cout << "\nNoua lista cu soferi:\n", sofer1->displayList(), cout << "\n";


	//Modificam datele angajatului 1 si ale angajatului 2 din lista
	angajat1->updateNume("Carmen", 1);
	angajat1->updateLocDeMunca("ZARA", 2);
	//Afisam noua lista modificata a angajatilor
	cout << "Lista cu angajati:" << "\n", angajat1->displayList(), cout << "\n";

	//Modificam datele parintelui de pe pozitia 0 si parintelui de pe pozitia 3
	p1->updateTipMasina("BMW", 0);
	p1->updateLocDeMunca("Facebook", 3);
	//Stergem parintele din pozitia 1
	p1->deleteParinte(1);
	//Afisam noua lista modificata a parintilor
	cout << "Lista cu parinti:\n", p1->displayList(), cout << "\n";

	system("pause");
	return 0;
}