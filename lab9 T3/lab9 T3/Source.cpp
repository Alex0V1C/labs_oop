#include <iostream>
#include <string>

using namespace std;

class Plant
{
protected:
	double Kcal;
	double vitaminA;
	double vitaminC;

public:
	Plant(double kcal, double vitaminA, double vitaminC)
	{
		this->Kcal = kcal;
		this->vitaminA = vitaminA;
		this->vitaminC = vitaminC;
	}

	virtual double Energy1() = 0;

	double getKcal()
	{
		return this->Kcal;
	}

	double getVitaminA()
	{
		return this->vitaminA;
	}

	double getVitaminC()
	{
		return this->vitaminC;
	}
};

class  FoodSource
{
protected:
	double carbohydrates;
	double protein;
	
public:
	FoodSource(double carbohydrates, double protein)
	{
		this->carbohydrates = carbohydrates;
		this->protein = protein;
	}

	virtual double Energy2() = 0;

	int getCarbohydrates()
	{
		return this->carbohydrates;
	}

	int getProtein()
	{
		return this->protein;
	} 
};

class Carrot : public Plant
{
protected:
	string variety; //Danvers, Nantes, Imperator;
public:
	Carrot(double kcal, double vitaminA, double vitaminB, string variety) :Plant(kcal, vitaminA, vitaminB) 
	{
		this->variety = variety;
	}

	double Energy1()
	{
		return (this->vitaminA + this->vitaminC) / this->getKcal();
	}

	using Plant::getKcal;
	using Plant::getVitaminA;
	using Plant::getVitaminC;

};

class Chicken : public FoodSource
{
private:
	int quantity;

public:
	Chicken(double carbohydrates, double protein, double quantity) : FoodSource(carbohydrates, protein)
	{
		this->quantity = quantity;
	}

	double Energy2()
	{
		return this->quantity * this->getProtein() / this->carbohydrates;
	}

	using FoodSource::getCarbohydrates;
	using FoodSource::getProtein;
};

class Apple :public Plant, public FoodSource
{
public:
	Apple(double kcal, double vitaminA, double vitaminB, double carbohydrates, double protein):Plant(kcal, vitaminA, vitaminB), FoodSource(carbohydrates, protein){}
	
	double Energy2()
	{
		return this->getProtein() / this->carbohydrates;
	}
	
	double Energy1()
	{
		return (this->vitaminA + this->vitaminC) / this->getKcal();
	}

	using Plant::getKcal;
	using Plant::getVitaminA;
	using Plant::getVitaminC;

	using FoodSource::getCarbohydrates;
	using FoodSource::getProtein;
};

int main()
{
	Carrot *carrot = new Carrot(60, 35, 20, "Danvers");
	cout << "Carrot: " << carrot->Energy1() << "\n";
	Plant *plant;
	plant = carrot;
	cout << "Carrot: " << plant->Energy1() << "\n";

	Chicken *chicken = new Chicken(120, 50, 30);
	cout << "Chicken: " << chicken->Energy2() << "\n";
	FoodSource *food;
	food = chicken;
	cout << "Chicken: " << food->Energy2() << "\n";

	Apple *apple = new Apple(79, 10, 12, 39, 18);
	cout << "Apple: " << apple->Energy1() << " " << apple->Energy2();
	//Nu putem folosi un pointer la o singura calsa de baza care ca functoneze cu ambele functii
	system("pause");
	return 0;
}