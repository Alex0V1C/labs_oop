#include <iostream>
#include <string>
#include <list>
using namespace std;

class Persoana
{
public:
	string nume, prenume;
	int varsta;

	Persoana() {}

	Persoana(string nume, string prenume, int varsta)
	{
		this->nume = nume;
		this->prenume = prenume;
		this->varsta = varsta;
	}

	static Persoana citire()
	{
		string nume, prenume;
		int varsta;

		cout << "Persoana " <<"\n";
		cout << "	Nume: ", cin >> nume, cout << "\n";
		cout << "	Prenume: ", cin >> prenume, cout << "\n";
		cout << "	Varsta: ", cin >> varsta;

		return Persoana(nume, prenume, varsta);
	}

	void afisare()
	{
		cout << this->nume << " " << this->prenume << " " << this->varsta << "\n";
	}
};

class Angajat : public Persoana
{
public :
	string angajator;

	Angajat() {}

	Angajat(string nume, string prenume, int varsta, string angajator) : Persoana(nume, prenume, varsta)
	{
		this->angajator = angajator;

	}

	static Angajat citire()
	{
		string nume, prenume, angajator;
		int varsta;

		cout << "Angajatul " << "\n";
		cout << "	Nume: ", cin >> nume, cout << "\n";
		cout << "	Prenume: ", cin >> prenume, cout << "\n";
		cout << "	Varsta: ", cin >> varsta;
		cout << "	Angajator: ", cin >> angajator;

		return Angajat(nume, prenume, varsta, angajator);
	}

	void afisare()
	{
		cout << this->nume << " " << this->prenume << " " << this->varsta << " " << this->angajator << "\n";
	}
};

class Parinte : public Angajat
{
public:
	int nrCopii;

	Parinte() {}

	Parinte(string nume, string prenume, int varsta, string angajator, int nrCopii) : Angajat(nume, prenume, varsta, angajator)
	{
		this->nrCopii = nrCopii;
	}

	static Parinte citire()
	{
		string nume, prenume, angajator;
		int varsta, nrCopii;

		cout << "Parintele " << "\n";
		cout << "	Nume: ", cin >> nume, cout << "\n";
		cout << "	Prenume: ", cin >> prenume, cout << "\n";
		cout << "	Varsta: ", cin >> varsta;
		cout << "	Angajator: ", cin >> angajator;
		cout << "	Nr. copii: ", cin >> nrCopii;
		
		return Parinte(nume, prenume, varsta, angajator, nrCopii);
	}

	void afisare()
	{
		cout << this->nume << " " << this->prenume << " " << this->varsta << " " << this->angajator << this->nrCopii << "\n";
	}
};

template <class T>
class Grup
{
public:
	int nr;

	list<T> grup;

	Grup(int nr)
	{	
		this->nr = nr;

		for (int i = 1; i <= this->nr; ++i)
		{
			grup.push_back(T::citire());
		}
	}

	void afisareGrup()
	{
		for (auto it = grup.begin(); it != grup.end(); ++it)
		{
			it->afisare();
		}
	}

};

void Menu()
{
	cout << "1. Lista de persoane\n";
	cout << "2. Lista de angajati\n";
	cout << "3. Lista de parinti\n";

	int tip;
	// tip = 1 => persoane
	// tip = 2 => angajati
	// tip = 3 => parinti

	cin >> tip;
	if (tip == 1)
	{
		Grup<Persoana> *p = new Grup<Persoana>(3);
		p->afisareGrup();
		delete p;
	}
	else if (tip == 2)
	{
		Grup<Angajat> *p = new Grup<Angajat>(3);
		p->afisareGrup();
		delete p;
	}
	else
	{
		Grup<Parinte> *p = new Grup<Parinte>(3);
		p->afisareGrup();
		delete p;
	}

}

int main()
{	
	Menu();
	system("pause");
	return 0;
}